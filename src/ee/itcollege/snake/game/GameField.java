package ee.itcollege.snake.game;

import java.util.Timer;
import java.util.TimerTask;

import ee.itcollege.snake.elements.Apple;
import ee.itcollege.snake.elements.Snake;
import ee.itcollege.snake.lib.CollisionDetector;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * The main game object that holds the gamefield
 */
public class GameField extends Pane {

	private Snake snake = new Snake();
	private Apple apple;
	private Timer timer = new Timer();

	public GameField() {
		getChildren().add(snake);
		Label label = new Label();
		label.setText("punktid siia");
		getChildren().add(label);
		Button button = new Button("tere");
		getChildren().add(button);
		
		createApple();
		
		timer.scheduleAtFixedRate(new TimerTask() {
		    @Override
		    public void run() {
		    	tick();
		    }
		}, 100, 100);
	}
	
	private void createApple() {
		
		Platform.runLater(() -> {
			if (null != apple) {
				getChildren().remove(apple);
			}
			
			int x = (int) (600 * Math.random());
			int y = (int) (500 * Math.random());
			apple = new Apple(x, y);
			getChildren().add(apple);
		});
	} 

	public void tick() {
		snake.move();
		if (snake.collidesWithItself()) {
			System.exit(0);
		}
		
		if (CollisionDetector.collide(snake.getHead(), apple)) {
			snake.eat(apple);
			createApple();
		}
	}
	

	public Snake getSnake() {
		return snake;
	}

}
