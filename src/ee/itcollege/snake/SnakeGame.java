package ee.itcollege.snake;

import ee.itcollege.snake.elements.Snake;
import ee.itcollege.snake.game.GameField;
import ee.itcollege.snake.lib.Direction;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class SnakeGame extends Application {

	GameField gameField = new GameField();

	@Override
	public void start(Stage window) throws Exception {

		Rectangle2D screenSize = Screen.getPrimary().getVisualBounds();
		Scene scene = new Scene(gameField,
				screenSize.getWidth() / 2,
				screenSize.getHeight() / 2,
				false,
				SceneAntialiasing.BALANCED);

		window.setScene(scene);

		window.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			
			Snake snake = gameField.getSnake();
			
			if (KeyCode.ESCAPE == event.getCode()) {
				System.exit(0);
			}
			else if (KeyCode.UP == event.getCode()) {
				snake.setDirection(Direction.UP);
			}
			else if (KeyCode.DOWN == event.getCode()) {
				snake.setDirection(Direction.DOWN);
			}
			else if (KeyCode.LEFT == event.getCode()) {
				snake.setDirection(Direction.LEFT);
			}
			else if (KeyCode.RIGHT == event.getCode()) {
				snake.setDirection(Direction.RIGHT);
			}
			
			if (event.isControlDown()) {
				snake.move();
			}
			
		});
		
//		PerspectiveCamera camera = new PerspectiveCamera(false);
//		camera.setFieldOfView(30);
//		camera.setNearClip(.1);
//		camera.setFarClip(1000);
//		camera.getTransforms().add(new Translate(0, -300, -200));
//		camera.getTransforms().add(new Rotate(300, Rotate.X_AXIS));
//		//camera.getTransforms().add(new Rotate(180, Rotate.Y_AXIS));
//		scene.setCamera(camera);
		
		window.setResizable(false);
		window.setTitle("Snake!");
		window.show();
		window.setOnCloseRequest(e -> System.exit(0));
	}

	public static void main(String[] args) {
		launch(args);
	}

}
