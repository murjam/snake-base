package ee.itcollege.snake.elements;

import javafx.scene.paint.Color;

public class SnakeHead extends SnakePart {

	public SnakeHead(int x, int y) {
		super(x, y);
		
		setFill(Color.BLACK);
	}

}
