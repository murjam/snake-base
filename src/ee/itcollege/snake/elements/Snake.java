package ee.itcollege.snake.elements;

import java.util.ArrayList;

import ee.itcollege.snake.lib.CollisionDetector;
import ee.itcollege.snake.lib.Direction;
import javafx.application.Platform;
import javafx.scene.Group;

public class Snake extends Group {

	private Direction direction = Direction.RIGHT;
	private ArrayList<SnakePart> parts = new ArrayList<SnakePart>();

	public Snake() {
		addPart(new SnakeHead(100, 100));
		
		for (int i = 0; i < 10; i++) {
			SnakePart part = new SnakePart(100, 100);
			addPart(part);
		}
	}

	private void addPart(SnakePart part) {
		getChildren().add(part);
		parts.add(part);
	}

	public void move() {
		// move all the parts but the head
		for (int i = parts.size() - 1; i >= 1; i--) {
			SnakePart part = (SnakePart) parts.get(i);
			SnakePart previousPart = (SnakePart) parts.get(i - 1);
			part.setX(previousPart.getX());
			part.setY(previousPart.getY());
		}

		// move the head
		SnakePart head = getHead();
		switch (direction) {
			case RIGHT:
				head.setX(head.getX() + SnakePart.SIZE);
				break;
			case LEFT:
				head.setX(head.getX() - SnakePart.SIZE);
				break;
			case UP:
				head.setY(head.getY() + SnakePart.SIZE);
				break;
			case DOWN:
				head.setY(head.getY() - SnakePart.SIZE);
				break;
		}

	}

	public SnakePart getHead() {
		return parts.get(0);
	}

	public Direction getDirection() {
		return direction;
	}
	
	public void setDirection(Direction direction) {
		boolean opposite = this.direction.isOpposite(direction);
		if (!opposite) {
			this.direction = direction;
		}
	}

	public boolean collidesWithItself() {
		
		SnakePart head = getHead();
		
		for (SnakePart part : parts) {
			if (part != head && CollisionDetector.collide(head, part)) {
				return true;
			}
		}
		
		return false;
	}

	public void eat(Apple apple) {
		Platform.runLater(() -> {
			addPart(new SnakePart(-100, -100));
		});
	}

}














