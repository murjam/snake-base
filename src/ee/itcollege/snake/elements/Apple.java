package ee.itcollege.snake.elements;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Apple extends Circle {

	public Apple(int x, int y) {
		setLayoutX(x);
		setLayoutY(y);
		setRadius(SnakePart.SIZE);
		setFill(Color.RED);
	}
	
}
