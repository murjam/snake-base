package ee.itcollege.snake.elements;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class SnakePart extends Rectangle {

	public static final int SIZE = 30;

	public SnakePart(int x, int y) {
		super(x, y, SIZE - 1, SIZE - 1);

		double red = Math.random() * .2 + .3;
		double green = Math.random() * .2 + .3;
		double blue = Math.random() * .2 + .7;
		setFill(new Color(red, green, blue, 1));
	}

}
